﻿using System.Linq;

namespace Interface_for_Arduino.Properties {
	
	
	// This class allows you to handle specific events on the settings class:
	//  The SettingChanging event is raised before a setting's value is changed.
	//  The PropertyChanged event is raised after a setting's value is changed.
	//  The SettingsLoaded event is raised after the setting values are loaded.
	//  The SettingsSaving event is raised before the setting values are saved.
	internal sealed partial class Settings
	{
		#region Properties

		private static bool _haveSettingsChanged;
		public static bool HaveSettingsChanged
		{
			get { return _haveSettingsChanged; }
			set { _haveSettingsChanged = value; }
		}

		#endregion // Properties



		#region Constructors

		public Settings() {
			// // To add event handlers for saving and changing settings, uncomment the lines below:

			this.SettingChanging += this.SettingChangingEventHandler;
			//this.PropertyChanged += PropertyChangedEventHandler;
			this.SettingsSaving += this.SettingsSavingEventHandler;
		}

		#endregion // Constructors



		#region Event Handlers

		private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e)
		{
			HaveSettingsChanged = true;
		}
		
		private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e)
		{
			HaveSettingsChanged = false;
		}

		#endregion // Event Handlers
	}
}
