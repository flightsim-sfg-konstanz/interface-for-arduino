﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

using System.Configuration;
using System.Windows;
using Interface_for_Arduino;

namespace Interface_for_Arduino.ViewModels
{
	public class MainWindowViewModel : ViewModelBase
	{
		#region Properties

		public static MainWindowViewModel Instance { get { return (MainWindowViewModel) Application.Current.MainWindow.DataContext; } }


		private ObservableCollection<TabViewModel> _tabs;
		public ObservableCollection<TabViewModel> Tabs
		{
			get
			{
				if (_tabs == null)
				{
					_tabs = new ObservableCollection<TabViewModel>();
					_tabs.CollectionChanged += OnTabsCollectionChanged;
					CreateDefaultTabs();
				}
				return _tabs;
			}
		}

		private int _activeTabIndex;
		public int ActiveTabIndex
		{
			get { return _activeTabIndex; }
			set
			{
				if (value != _activeTabIndex)
				{
					_activeTabIndex = value;
				}

				base.OnPropertyChanged("ActiveTabIndex");
			}
		}

		private ReadOnlyCollection<CommandViewModel> _tabCommands;
		/// <summary>
		/// Returns a read-only list of commands 
		/// that the UI can display and execute.
		/// </summary>
		public ReadOnlyCollection<CommandViewModel> TabCommands
		{
			get
			{
				if (_tabCommands == null)
				{
					List<CommandViewModel> cmds = this.CreateCommands();
					_tabCommands = new ReadOnlyCollection<CommandViewModel>(cmds);
				}
				return _tabCommands;
			}
		}

		#endregion // Properties



		#region Constructors

		public MainWindowViewModel()
		{
			DisplayName = "Arduinator X3000 Pro Ultimate Deluxe Special Limited Edition";
			var path = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
		}

		#endregion // Constructors



		#region Methods

		#region EventHandlers

		private void OnTabsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.OldItems != null)
			{
				foreach (TabViewModel tab in e.OldItems)
				{
					tab.Dispose();
				}
			}
		}

		#endregion // EventHandlers

		#region Private Helpers

		#region Init stuff

		private void CreateDefaultTabs()
		{
			Tabs.Add(new HomeViewModel());
		}

		private List<CommandViewModel> CreateCommands()
		{
			return new List<CommandViewModel>
			{
				new CommandViewModel(
					"Arduino-Interface",
					new RelayCommand(param => this.OpenArduino_InterfaceTabExecute(), param => CanOpenArduino_InterfaceTabExecute())),
				new CommandViewModel(
					"Tetris",
					new RelayCommand(param => this.OpenTetrisTabExecute(), param => CanOpenTetrisTabExecute())),
				new CommandViewModel(
					"FlightSim",
					new RelayCommand(param => this.OpenFlightSimTabExecute(), param => CanOpenFlightSimTabExecute()))
			};
		}

		#endregion // Init stuff

		private int CountTabClassInstancesInTabs(TabViewModel tab)
		{
			int count = 0;
			for (int i = 0; i < Tabs.Count; i++)
			{
				if (Tabs.ElementAt(i).GetType().Name == tab.GetType().Name)
					count++;
			}

			return count;
		}
		private int CountTabClassInstancesInTabs(string typeName)
		{
			int count = 0;
			for (int i = 0; i < Tabs.Count; i++)
			{
				if (Tabs.ElementAt(i).GetType().Name == typeName)
					count++;
			}

			return count;
		}

		private int IndexOfInstance(TabViewModel tab)
		{
			for (int i = 0; i < Tabs.Count; i++)
			{
				if (Tabs.ElementAt(i).Equals(tab))
					return i;
			}

			for (int i = 0; i < Tabs.Count; i++)
			{
				if (Tabs.ElementAt(i).GetType().Name == tab.GetType().Name)
					return i;
			}

			return -1;
		}

		private void SetActiveTab(TabViewModel tab)
		{
			int index = IndexOfInstance(tab);
			if (index != -1)
				ActiveTabIndex = index;
		}

		#endregion // Private Helpers

		public TabViewModel[] getTabByName(string tab)
		{
			List<TabViewModel> viewModels = new List<TabViewModel>();
			for (int i = 0; i < Tabs.Count; i++)
			{
				string typeString = Tabs[i].GetType().ToString();
				if (Tabs[i].GetType().ToString().Contains(tab))
				{
					viewModels.Add(Tabs[i]);
				}
			}

			// Return
			if (viewModels.Count != 0)
			{
				return viewModels.ToArray();
			}
			else
			{
				return null;
			}
		}

		#endregion // Methods



		#region Commands

		public ICommand WindowClosing { get { return new RelayCommand(param => WindowClosingExecute()); } }
		void WindowClosingExecute()
		{
			OnDispose();
		}

		public ICommand OpenHomeTab { get { return new RelayCommand(param => OpenHomeTabExecute()); } }
		void OpenHomeTabExecute()
		{
			TabViewModel tab = new HomeViewModel();

			if (TabViewModel.IsCountInstancesInRange(HomeViewModel.MaxTabInstances, CountTabClassInstancesInTabs(tab) + 1))
			{
				this.Tabs.Insert(0, tab);
			}

			SetActiveTab(tab);
		}


		// Tab open commands

		void OpenArduino_InterfaceTabExecute()
		{
			TabViewModel tab = new Arduino_InterfaceViewModel();
			this.Tabs.Add(tab);
			if (Properties.Settings.Default.AutoOpenTabs)
				SetActiveTab(tab);
		}
		bool CanOpenArduino_InterfaceTabExecute()
		{
			if (TabViewModel.IsCountInstancesInRange(Arduino_InterfaceViewModel.MaxTabInstances, CountTabClassInstancesInTabs("Arduino_InterfaceViewModel") + 1))
				return true;
			else
				return false;
		}

		void OpenTetrisTabExecute()
		{
			TabViewModel tab = new TetrisViewModel();
			this.Tabs.Add(tab);
			if (Properties.Settings.Default.AutoOpenTabs)
				SetActiveTab(tab);
		}
		bool CanOpenTetrisTabExecute()
		{
			if (TabViewModel.IsCountInstancesInRange(TetrisViewModel.MaxTabInstances, CountTabClassInstancesInTabs("TetrisViewModel") + 1))
				return true;
			else
				return false;
		}

		void OpenFlightSimTabExecute()
		{
			TabViewModel tab = new FlightSimViewModel();
			this.Tabs.Add(tab);
			if (Properties.Settings.Default.AutoOpenTabs)
				SetActiveTab(tab);
		}
		bool CanOpenFlightSimTabExecute()
		{
			if (TabViewModel.IsCountInstancesInRange(FlightSimViewModel.MaxTabInstances, CountTabClassInstancesInTabs("FlightSimViewModel") + 1))
				return true;
			else
				return false;
		}

		#endregion // Commands



		#region Dispose

		protected override void OnDispose()
		{
			for (int i = 0; i < Tabs.Count; i++)
			{
				Tabs[i].Dispose();
			}
		}

		#endregion // Dispose
	}
}
