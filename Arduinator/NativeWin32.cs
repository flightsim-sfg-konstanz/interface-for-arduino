﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Interface_for_Arduino
{
	// Code derived from Ali Zamurad, 12 Apr 2007, Codeproject
	// Extended by me
	class NativeWin32
	{
		public const uint KEYEVENTF_EXTENDEDKEY = 0x0001;
		public const uint KEYEVENTF_KEYUP = 0x0002;

		[DllImport("user32.dll")]
		public static extern int FindWindow(
			string lpClassName, // class name 
			string lpWindowName // window name 
		);

		[DllImport("user32.dll")]
		public static extern int SetForegroundWindow(
			IntPtr hWnd // handle to window
		);

		[DllImport("user32")]
		public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);

		[DllImport("user32")]
		public static extern short VkKeyScan(char ch);
	}
}
