﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

using Interface_for_Arduino.Properties;
using System.Windows.Data;

namespace Interface_for_Arduino.ViewModels
{
	public class HomeViewModel : TabViewModel
	{
		#region Properties

		private static int _maxTabInstances;
		public static int MaxTabInstances
		{
			get { return _maxTabInstances; }
			private set
			{
				if (value <= -1)
					_maxTabInstances = (int)MaxTabInstancesDefinitions.None;
				else
					_maxTabInstances = value;
			}
		}

		#endregion // Properties



		#region Constructors

		static HomeViewModel()
		{
			MaxTabInstances = (int) MaxTabInstancesDefinitions.OnlyOne;
		}

		public HomeViewModel()
		{
			base.DisplayName = "Home";
			HomeTabCloseableChanged.Execute(!Properties.Settings.Default.HomeTabCloseButtonEnabled);
		}

		#endregion // Constructors



		#region Commands

		public ICommand HomeTabCloseableChanged { get { return new RelayCommand(param => HomeTabCloseableChangedExecute((bool) param)); } }

		private void HomeTabCloseableChangedExecute(bool state)
		{
			TabCloseButtonEnabled = !state;
		}

		public ICommand SaveSettings { get { return new RelayCommand(param => SaveSettingsExecute(), param => CanSaveSettingsExecute()); } }

		private void SaveSettingsExecute()
		{
			Properties.Settings.Default.Save();
		}
		private bool CanSaveSettingsExecute()
		{
			if (Settings.HaveSettingsChanged)
				return true;
			else
				return false;
		}

		public ICommand CancelSettings { get { return new RelayCommand(param => CancelSettingsExecute(), param => CanCancelSettingsExecute()); } }

		private void CancelSettingsExecute()
		{
			Properties.Settings.Default.Reload();
		}
		private bool CanCancelSettingsExecute()
		{
			if (Settings.HaveSettingsChanged)
				return true;
			else
				return false;
		}

		#endregion // Commands
	}
}
