﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Threading;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;

using ArduinoInterface;
using System.IO.Ports;


namespace Interface_for_Arduino.ViewModels
{
	[Export]
	[PartCreationPolicy(CreationPolicy.Shared)]
	public class Arduino_InterfaceViewModel : TabViewModel
	{
		#region Properties

		#region Static Properties

		private static int _maxTabInstances;

		public static int MaxTabInstances
		{
			get { return _maxTabInstances; }
			private set
			{
				if (value <= -1)
					_maxTabInstances = (int) MaxTabInstancesDefinitions.None;
				else
					_maxTabInstances = value;
			}
		}

		private MTObservableCollection<ArduinoViewModel> _arduinoCollection;

		public MTObservableCollection<ArduinoViewModel> ArduinoCollection
		{
			get { return _arduinoCollection; }
			private set { _arduinoCollection = value; OnPropertyChanged("ArduinoCollection"); }
		}

		private ConsoleViewModel _arduinoConsole;

		public ConsoleViewModel ArduinoConsole
		{
			get { return _arduinoConsole; }
			set { _arduinoConsole = value; OnPropertyChanged("ArduinoConsole"); }
		}

		private String _arduinoSendMessage;

		public String ArduinoSendMessage
		{
			get { return _arduinoSendMessage; }
			set { _arduinoSendMessage = value; OnPropertyChanged("ArduinoSendMessage"); }
		}

		#endregion // Static Properties

		#endregion // Properties



		#region Constructors

		static Arduino_InterfaceViewModel()
		{
			MaxTabInstances = (int) MaxTabInstancesDefinitions.OnlyOne;
		}

		public Arduino_InterfaceViewModel()
		{
			base.DisplayName = "Arduino-Interface";
			ArduinoSendMessage = "";
			ArduinoCollection = new MTObservableCollection<ArduinoViewModel>();
			ArduinoConsole = new ConsoleViewModel();

			// Register Events
			Arduino.ArduinoAvailable += OnArduinoAvailable;
			Arduino.ArduinoDisconnected += OnArduinoDisconnected;

			// Enable the Automatic Listener
			Arduino.EnableArduinoListener = true;
		}

		#endregion // Constructors



		#region Methods

		#region Arduino Event Methods

		private void OnMessageRecieved(object source, ArduinoInterface.ArduinoEventArgs args)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				processMessageRecieved(source, args);
				relayMessageRecieved(source, args);
			}));
		}

		private void processMessageRecieved(object source, ArduinoInterface.ArduinoEventArgs args)
		{
			if (!args.Command.IsFaulty)
			{
				switch (args.Command.Type)
				{
					case ArduinoCommand.CommandTypes.Arduino_To_Arduino:
						// Print Message
						ArduinoConsole.AddConsoleText("Arduino to Arduino: @nobreak", ConsoleViewModel.TextColors.Turquoise);
						// Relay to the target Arduino
						if (ArduinoCollection.Any(p => p.ArduinoName == args.Command.Target))
						{
							try
							{
								// Send
								ArduinoCollection.Single(p => p.ArduinoName == args.Command.Target).Send(args.Command);
							}
							catch (Exception e)
							{
								PrintError("Arduino .Send() has failed\n\t" + e.Message);
							}

							ArduinoConsole.AddConsoleText(String.Format("»{0}«", args.Command));
						}
						else
						{
							PrintWarning(String.Format("Ziel-Arduino »{0}« konnte nicht gefunden werden. Origin: »{1}«", args.Command.Target, args.Command.Origin));
						}

						break;



					case ArduinoCommand.CommandTypes.Arduino_To_Interface:

						if (args.Command.Content == "This is test data with 32 chars.")
						{
							ArduinoCollection[GetArduinoIndex(args.Command.Origin)].StopPingmeasureTime();
						}
						else if (args.Command.Content.Contains('$'))
						{
							PrintWarning(String.Format("»{0}« notifies that {1}",
								args.Command.Origin,
								args.Command.Content.Replace("$", "").Replace("(", "»").Replace(")", "«")));
						}
						else
						{
							ArduinoConsole.AddConsoleText("Arduino to Interface: @nobreak", ConsoleViewModel.TextColors.Blue);
							ArduinoConsole.AddConsoleText(String.Format("»{0}«", args.Command));
						}

						break;



					case ArduinoCommand.CommandTypes.Arduino_Broadcast:
						for (int i = 0; i < ArduinoCollection.Count; i++)
						{
							if (ArduinoCollection[i].ArduinoName != args.Command.Origin)
							{
								try
								{
									ArduinoCollection[i].Send(args.Command);
								}
								catch (Exception e)
								{
									PrintError("Warning Arduino .Send() has failed\n\t" + e.Message);
								}
							}
						}

						ArduinoConsole.AddConsoleText("Arduino Broadcast: @nobreak", ConsoleViewModel.TextColors.Violet);
						ArduinoConsole.AddConsoleText(String.Format("»{0}«", args.Command));

						break;

					default:
						break;
				}
			}
		}

		private void relayMessageRecieved(object source, ArduinoInterface.ArduinoEventArgs args)
		{
			if (args.Command.Content == TetrisViewModel.moveLeft.Content)
			{
				TabViewModel[] viewModels = MainWindowViewModel.Instance.getTabByName("TetrisViewModel");
				if (viewModels != null)
				{
					foreach (var item in viewModels)
					{
						((TetrisViewModel) item).commandMoveLeft(); 
					}
				}
			}
			else if (args.Command.Content == TetrisViewModel.moveRight.Content)
			{
				TabViewModel[] viewModels = MainWindowViewModel.Instance.getTabByName("TetrisViewModel");
				if (viewModels != null)
				{
					foreach (var item in viewModels)
					{
						((TetrisViewModel)item).commandMoveRight();
					}
				}
			}
			else if (args.Command.Content == TetrisViewModel.moveDown.Content)
			{
				TabViewModel[] viewModels = MainWindowViewModel.Instance.getTabByName("TetrisViewModel");
				if (viewModels != null)
				{
					foreach (var item in viewModels)
					{
						((TetrisViewModel)item).commandMoveDown();
					}
				}
			}
			else if (args.Command.Content == TetrisViewModel.turnLeft.Content)
			{
				TabViewModel[] viewModels = MainWindowViewModel.Instance.getTabByName("TetrisViewModel");
				if (viewModels != null)
				{
					foreach (var item in viewModels)
					{
						((TetrisViewModel)item).commandTurnLeft();
					}
				}
			}
			else if (args.Command.Content == TetrisViewModel.turnRight.Content)
			{
				TabViewModel[] viewModels = MainWindowViewModel.Instance.getTabByName("TetrisViewModel");
				if (viewModels != null)
				{
					foreach (var item in viewModels)
					{
						((TetrisViewModel)item).commandTurnRight();
					}
				}
			}

			else if (args.Command.Content == FlightSimViewModel.processed.Content)
			{
				TabViewModel[] viewModels = MainWindowViewModel.Instance.getTabByName("FlightSimViewModel");
				if (viewModels != null)
				{
					foreach (var item in viewModels)
					{
						((FlightSimViewModel)item).IsAirspeedProcessed = true;
					}
				}
			}
		}

		private void OnArduinoAvailable(ArduinoChangedEventArgs args)
		{
			// Print console
			ArduinoConsole.AddConsoleText("Arduino connected: @nobreak", ConsoleViewModel.TextColors.Green);
			ArduinoConsole.AddConsoleText(String.Format("COM-Port \"{0}\" - Name {1}", args.ComPort, args.Name));

			try
			{
				ArduinoCollection.Add(new ArduinoViewModel(args.Name, args.ComPort, args.sp, OnMessageRecieved));
			}
			catch (Exception e)
			{
				//PrintError(e.ToString());

				if (ContainsArduino(args.Name))
				{
					ArduinoCollection.Remove(ArduinoCollection[GetArduinoIndex(args.Name)]);
				}

				//PrintConnectedArduinos();
				return;
			}

			//Task.Factory.StartNew(WaitArduinoReset).Wait();

			if (ContainsArduino(args.Name))
			{
				try
				{
					ArduinoCollection[GetArduinoIndex(args.Name)].Send("Connection established");
				}
				catch (Exception)
				{
					//PrintError("Connection established command was not sent");
					ArduinoCollection[GetArduinoIndex(args.Name)].Dispose();
				}
			}
		}

		private void OnArduinoDisconnected(ArduinoChangedEventArgs args)
		{
			// Print console
			ArduinoConsole.AddConsoleText("Arduino disconnected: @nobreak", ConsoleViewModel.TextColors.Magenta);
			ArduinoConsole.AddConsoleText(String.Format("COM-Port \"{0}\" - Name {1}", args.ComPort, args.Name));

			if (ContainsArduino(args.Name))
			{
				ArduinoCollection[GetArduinoIndex(args.Name)].Dispose();
				ArduinoCollection.Remove(ArduinoCollection[GetArduinoIndex(args.Name)]);

				Console.Write(" on COM-Port \"{0}\" with name {1}.", args.ComPort, args.Name);
			}
		}

		#endregion // Arduino Event Methods

		private bool SendBroadcastInternally(string message)
		{
			// Print Interface Broadcast message
			if (ArduinoCollection.Count == 1)
			{
				ArduinoConsole.AddConsoleText("Interface Broadcast: @nobreak", ConsoleViewModel.TextColors.Orange);
			}
			else
			{
				//ArduinoConsole.AddParagraph();
				ArduinoConsole.AddConsoleText("Interface Broadcast:", ConsoleViewModel.TextColors.Orange);
			}
			

			ArduinoCommand tmpCommand;
			for (int i = 0; i < ArduinoCollection.Count; i++)
			{
				tmpCommand = new ArduinoCommand(ArduinoCommand.CommandTypes.Interface_Broadcast, "unspecified", message, "Interface");

				// Print commands to console
				if (ArduinoCollection.Count == 1)
					ArduinoConsole.AddConsoleText(String.Format("»{0}«", tmpCommand.ParseString()));
				else
					ArduinoConsole.AddConsoleText(String.Format("\t»{0}«", tmpCommand.ParseString()));

				// Send the actual commannd
				try
				{
					ArduinoCollection[i].Send(tmpCommand);
				}
				catch (Exception e)
				{
					return false;
				}
			}

			return true;
		}

		public void SendSingle(int sendIndex, string message)
		{
			if (ArduinoCollection[sendIndex].Send(message))
			{
				ArduinoCommand tmpCommand = new ArduinoCommand(ArduinoCommand.CommandTypes.Interface_To_Arduino, ArduinoCollection[sendIndex].ArduinoName, ArduinoSendMessage, "Interface");
				ArduinoConsole.AddConsoleText("Interface to Arduino: @nobreak", ConsoleViewModel.TextColors.Cyan);
				ArduinoConsole.AddConsoleText(String.Format("»{0}«", tmpCommand.ParseString()));
			}
			else
				PrintWarning("Problem occured during sending user command.");
		}
		public void SendBroadcast(int index, string message)
		{
			if (ArduinoBroadcast.CanExecute(null))
			{
				ArduinoBroadcast.Execute(null);
			}
		}

		#region Private ArduinoCollection Helpers

		private bool ContainsArduino(string name)
		{
			for (int i = 0; i < ArduinoCollection.Count; i++)
			{
				if (ArduinoCollection[i].ArduinoName == name)
					return true;
			}

			return false;
		}

		public int GetArduinoIndex(string name)
		{
			for (int i = 0; i < ArduinoCollection.Count; i++)
			{
				if (ArduinoCollection[i].ArduinoName == name)
					return i;
			}

			return -1;
		}

		#endregion // Private ArduinoCollection Helpers

		#region Private Console Helpers

		private void PrintError(string errorMessage)
		{
			if (errorMessage == "" || errorMessage == null)
			{
				ArduinoConsole.AddConsoleText("Error", ConsoleViewModel.TextColors.Red, FontWeights.Bold, FontStyles.Normal);
			}
			else
			{
				ArduinoConsole.AddConsoleText("Error: @nobreak", ConsoleViewModel.TextColors.Red, FontWeights.Bold, FontStyles.Normal);
				ArduinoConsole.AddConsoleText(errorMessage);
			}
		}

		private void PrintWarning(string warningMessage)
		{
			if (warningMessage == "" || warningMessage == null)
			{
				ArduinoConsole.AddConsoleText("Warning", ConsoleViewModel.TextColors.Yellow, FontWeights.Bold, FontStyles.Normal);
			}
			else
			{
				ArduinoConsole.AddConsoleText("Warning: @nobreak", ConsoleViewModel.TextColors.Yellow, FontWeights.Bold, FontStyles.Normal);
				ArduinoConsole.AddConsoleText(warningMessage);
			}
		}

		#endregion // Private Console Helpers

		#endregion // Methods



		#region Commands

		public ICommand ArduinoSend { get { return new RelayCommand(param => ArduinoSendExecute((int) param), param => CanArduinoSendExecute((int) param)); } }

		private void ArduinoSendExecute(int sendIndex)
		{
			if (ArduinoCollection[sendIndex].Send(ArduinoSendMessage))
			{
				ArduinoCommand tmpCommand = new ArduinoCommand(ArduinoCommand.CommandTypes.Interface_To_Arduino, ArduinoCollection[sendIndex].ArduinoName, ArduinoSendMessage, "Interface");
				ArduinoConsole.AddConsoleText("Interface to Arduino: @nobreak", ConsoleViewModel.TextColors.Cyan);
				ArduinoConsole.AddConsoleText(String.Format("»{0}«", tmpCommand.ParseString()));
			}
			else
				PrintWarning("Problem occured during sending user command.");
		}

		private bool CanArduinoSendExecute(int sendIndex)
		{
			if (sendIndex <= ArduinoCollection.Count - 1 && sendIndex >= 0 && ArduinoSendMessage != "")
				return true;
			else
				return false;
		}

		public ICommand ArduinoBroadcast { get { return new RelayCommand(param => ArduinoBroadcastExecute(), param => CanArduinoBroadcastExecute()); } }

		private void ArduinoBroadcastExecute()
		{
			if (!SendBroadcastInternally(ArduinoSendMessage))
			{
				PrintWarning("Problem occured during sending user broadcast.");
			}
		}

		private bool CanArduinoBroadcastExecute()
		{
			if (ArduinoCollection.Count > 0 && ArduinoSendMessage != "")
				return true;
			else
				return false;
		}

		#endregion // Commands



		#region Dispose

		protected override void OnDispose()
		{
			// Dispose each Arduino
			for (int i = 0; i < ArduinoCollection.Count; i++)
			{
				ArduinoCollection[i].Dispose();
			}

			// Unregister Events
			Arduino.ArduinoAvailable -= OnArduinoAvailable;
			Arduino.ArduinoDisconnected -= OnArduinoDisconnected;

			// Set the Listener off
			Arduino.EnableArduinoListener = false;
		}

		#endregion // Dispose
	}
}