﻿using Interface_for_Arduino.ViewModels;
using Microsoft.FlightSimulator.SimConnect;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Interop;
using System.Windows;
using ArduinoInterface;
using System.Windows.Threading;

namespace Interface_for_Arduino.ViewModels
{
	class FlightSimViewModel : TabViewModel
	{
		#region Fields

		private DispatcherTimer _updatePing;

		private SimConnect _simconnect = null;
		private const int WM_USER_SIMCONNECT = 0x0402;

		enum DEFINITIONS
		{
			Struct1,
		}

		enum DATA_REQUESTS
		{
			REQUEST_1,
		};

		// this is how you declare a data structure so that
		// simconnect knows how to fill it/read it.
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
		struct Struct1
		{
			// this is how you declare a fixed size string
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public String title;
			public double latitude;
			public double longitude;
			public double altitude;
			public double indicatedAirspeed;
		}; 
		
		// ArduinoCommands
		public static readonly ArduinoCommand processed = new ArduinoCommand(ArduinoCommand.CommandTypes.Arduino_To_Interface, "Interface", "Airspeed processed", "Airspeed-Indicator");

		#endregion // Fields



		#region Properties

		#region Static Properties

		private static int _maxTabInstances;
		public static int MaxTabInstances
		{
			get { return _maxTabInstances; }
			private set
			{
				if (value < -1)
					_maxTabInstances = (int) MaxTabInstancesDefinitions.OnlyOne;
				else
					_maxTabInstances = value;
			}
		}

		#endregion // Static Properties

		private bool _isFlightSimConnected;
		public bool IsFlightSimConnected
		{
			get { return _isFlightSimConnected; }
			private set { _isFlightSimConnected = value; OnPropertyChanged("IsFlightSimConnected"); }
		}

		private double _fsAirspeed;
		private double _lastTransmittedAirspeed;
		public double Airspeed
		{
			get { return _fsAirspeed; }
			private set { _fsAirspeed = value; OnPropertyChanged("Airspeed"); }
		}

		private bool _isAirspeedProcessed;
		public bool IsAirspeedProcessed
		{
			get { return _isAirspeedProcessed; }
			set { _isAirspeedProcessed = value; }
		}


		#endregion // Properties



		#region Constructors

		static FlightSimViewModel()
		{
			MaxTabInstances = (int) MaxTabInstancesDefinitions.OnlyOne;
		}

		public FlightSimViewModel()
		{
			base.DisplayName = "FlightSim";
			Airspeed = 0.0;

			// Intitialise timer for updating ping
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				_updatePing = new DispatcherTimer();
			}));

			_updatePing.Interval = new TimeSpan(0, 0, 0, 0, 40);
			_updatePing.Tick += OnUpdatePingTick;
			_updatePing.Start();

			OpenSimconnect();

			// define a data structure
			_simconnect.AddToDataDefinition(DEFINITIONS.Struct1, "Title", null, SIMCONNECT_DATATYPE.STRING256, 0.0f, SimConnect.SIMCONNECT_UNUSED);
			_simconnect.AddToDataDefinition(DEFINITIONS.Struct1, "Plane Latitude", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
			_simconnect.AddToDataDefinition(DEFINITIONS.Struct1, "Plane Longitude", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
			_simconnect.AddToDataDefinition(DEFINITIONS.Struct1, "Plane Altitude", "feet", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
			_simconnect.AddToDataDefinition(DEFINITIONS.Struct1, "AIRSPEED INDICATED", "knots", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);

			// IMPORTANT: register it with the simconnect managed wrapper marshaller
			// if you skip this step, you will only receive a uint in the .dwData field.
			_simconnect.RegisterDataDefineStruct<Struct1>(DEFINITIONS.Struct1);

			// catch a simobject data request
			_simconnect.OnRecvSimobjectData += new SimConnect.RecvSimobjectDataEventHandler(simconnect_OnRecvSimobjectData);

			_simconnect.RequestDataOnSimObject(DATA_REQUESTS.REQUEST_1, DEFINITIONS.Struct1, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.VISUAL_FRAME, SIMCONNECT_DATA_REQUEST_FLAG.DEFAULT, 0, 0, 0);
		}

		#endregion // Constructors



		#region Methods

		private void OpenSimconnect()
		{
			WindowInteropHelper lWih = new WindowInteropHelper(Application.Current.MainWindow);
			IntPtr lHwnd = lWih.Handle;

			HwndSource gHs = HwndSource.FromHwnd(lHwnd);
			gHs.AddHook(new HwndSourceHook(ProcessSimConnectWin32Events));

			try
			{
				_simconnect = new SimConnect("Managed Data Request", lWih.Handle, WM_USER_SIMCONNECT, null, 0);
			}
			catch (COMException ex)
			{ throw new Exception(); }

			if (_simconnect == null)
			{
				IsFlightSimConnected = false;
			}
			else
			{
				IsFlightSimConnected = true;
			}
		}

		private void CloseSimconnect()
		{
			if (_simconnect != null)
			{
				_simconnect.Dispose();
				_simconnect = null;
				IsFlightSimConnected = false;
			}
		}

		#region EventHandlers

		private IntPtr ProcessSimConnectWin32Events(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
		{
			if (IsFlightSimConnected && _simconnect != null)
			{
				_simconnect.ReceiveMessage();
			}

			return (IntPtr)0;
		}

		private void OnUpdatePingTick(object sender, EventArgs e)
		{
			TabViewModel[] viewModels = MainWindowViewModel.Instance.getTabByName("Arduino_InterfaceViewModel");
			if (viewModels != null)
			{
				foreach (var item in viewModels)
				{
					Arduino_InterfaceViewModel instance = (Arduino_InterfaceViewModel)item;
					int arduinoIndex = instance.GetArduinoIndex("Airspeed-Indicator");

					if (arduinoIndex != -1)
					{
						instance.SendSingle(arduinoIndex, Airspeed + "");
					}
				}
			}
		}

		#endregion // Event Handlers

		private void simconnect_OnRecvSimobjectData(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA data)
		{
			switch ((DATA_REQUESTS)data.dwRequestID)
			{
				case DATA_REQUESTS.REQUEST_1:
					Struct1 s1 = (Struct1)data.dwData[0];
					Airspeed = s1.indicatedAirspeed;
					break;

				default:
					//Console.WriteLine("Unknown request ID: " + data.dwRequestID);
					break;
			}
		}

		#endregion // Methods



		#region Commands
		#endregion // Commands



		#region Dispose

		protected override void OnDispose()
		{
			CloseSimconnect();

			// Remove ping event handler
			_updatePing.Tick -= OnUpdatePingTick;
		}

		#endregion // Dispose
	}
}
