﻿using System;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Interface_for_Arduino.ViewModels
{
	public class ConsoleViewModel : ViewModelBase
	{
		#region Fields

		public enum TextColors
		{
			Magenta,		// Disconnected
			Cyan,			// I-A
			Blue,			// A-I
			Orange,			// Broadcast
			Red,			// Errors
			Violet,			// A-B
			Green,			// Connected
			Turquoise,		// A-A
			Yellow			// Warnings
		}

		#endregion // Fields



		#region Properties

		private FlowDocument _document;

		public FlowDocument ConsoleText
		{
			get { return _document; }
			private set { _document = value; OnPropertyChanged("ConsoleText"); }
		}

		#endregion // Properties



		#region Constructors

		public ConsoleViewModel()
		{
			ConsoleText = new FlowDocument();
		}

		#endregion // Constructors



		#region Methods

		#region AddText

		public void AddConsoleText(string text)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				// Get last paragraph
				Paragraph paragraph = (Paragraph)ConsoleText.Blocks.LastBlock;
				if (paragraph == null)
				{
					// Add new paragraph if empty
					paragraph = new Paragraph();
					ConsoleText.Blocks.Add(paragraph);
				}

				// Format text
				if (text.EndsWith("@nobreak"))
					text = text.Replace("@nobreak", String.Empty);
				else
					text += "\n";

				// Add text to paragraph
				paragraph.Inlines.Add(new Run(text));
			}));
			OnPropertyChanged("ConsoleText");
		}

		public void AddConsoleText(string text, TextColors color)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				// Get last paragraph
				Paragraph paragraph = (Paragraph)ConsoleText.Blocks.LastBlock;
				if (paragraph == null)
				{
					// Add new paragraph if empty
					paragraph = new Paragraph();
					ConsoleText.Blocks.Add(paragraph);
				}

				// Format text
				if (text.EndsWith("@nobreak"))
					text = text.Replace("@nobreak", String.Empty);
				else
					text += "\n";

				// Add text to paragraph
				paragraph.Inlines.Add(new Run(text) { Foreground = GetColorValue(color)});
				OnPropertyChanged("ConsoleText");
			}));
		}

		public void AddConsoleText(string text, TextColors color, FontWeight fontWeight, FontStyle fontStyle)
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				// Get last paragraph
				Paragraph paragraph = (Paragraph)ConsoleText.Blocks.LastBlock;
				if (paragraph == null)
				{
					// Add new paragraph if empty
					paragraph = new Paragraph();
					ConsoleText.Blocks.Add(paragraph);
				}

				// Format text
				if (text.EndsWith("@nobreak"))
					text = text.Replace("@nobreak", String.Empty);
				else
					text += "\n";

				// Add text to paragraph
				paragraph.Inlines.Add(new Run(text)
				{
					Foreground = GetColorValue(color),
					FontWeight = fontWeight,
					FontStyle = fontStyle
				});
				OnPropertyChanged("ConsoleText");
			}));
		}

		#endregion // AddText

		public void AddParagraph()
		{
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				if (ConsoleText.Blocks.LastBlock != null)
				{
					//if (((Paragraph)ConsoleText.Blocks.LastBlock).Inlines.LastInline.Equals((Inline) new Run("")))
					//{
						ConsoleText.Blocks.Add(new Paragraph());
					//} 
				}
				OnPropertyChanged("ConsoleText");
			}));
		}

		#region Private Helpers

		private SolidColorBrush GetColorValue(TextColors color)
		{
			switch (color)
			{
				case TextColors.Magenta:
					return new SolidColorBrush(Color.FromRgb(220, 50, 240));
				case TextColors.Cyan:
					return new SolidColorBrush(Color.FromRgb(0, 174, 239));
				case TextColors.Blue:
					return new SolidColorBrush(Color.FromRgb(10, 120, 255));
				case TextColors.Orange:
					return new SolidColorBrush(Color.FromRgb(255, 110, 0));
				case TextColors.Red:
					return new SolidColorBrush(Color.FromRgb(255, 55, 55));
				case TextColors.Violet:
					return new SolidColorBrush(Color.FromRgb(150, 50, 240));
				case TextColors.Green:
					return new SolidColorBrush(Color.FromRgb(30, 230, 30));
				case TextColors.Turquoise:
					return new SolidColorBrush(Color.FromRgb(30, 230, 145));
				case TextColors.Yellow:
					return new SolidColorBrush(Color.FromRgb(230, 215, 30));
				default:
					return new SolidColorBrush(Color.FromRgb(0, 0, 0));
			}
		}

		#endregion // Private Helpers

		#endregion // Methods
	}
}
