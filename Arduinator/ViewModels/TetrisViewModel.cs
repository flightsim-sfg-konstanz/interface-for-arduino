﻿using ArduinoInterface;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Interface_for_Arduino.ViewModels
{
	public class TetrisViewModel : TabViewModel
	{
		#region Fields

		// Process to reference to Tetris
		private Process process = new Process();
		
		// ArduinoCommands
		public static readonly ArduinoCommand moveLeft = new ArduinoCommand(ArduinoCommand.CommandTypes.Arduino_To_Interface, "Interface", "Tetris.moveLeft", "Controller-Arduino");
		public static readonly ArduinoCommand moveRight = new ArduinoCommand(ArduinoCommand.CommandTypes.Arduino_To_Interface, "Interface", "Tetris.moveRight", "Controller-Arduino");
		public static readonly ArduinoCommand moveDown = new ArduinoCommand(ArduinoCommand.CommandTypes.Arduino_To_Interface, "Interface", "Tetris.moveDown", "Controller-Arduino");
		public static readonly ArduinoCommand turnLeft = new ArduinoCommand(ArduinoCommand.CommandTypes.Arduino_To_Interface, "Interface", "Tetris.turnLeft", "Controller-Arduino");
		public static readonly ArduinoCommand turnRight = new ArduinoCommand(ArduinoCommand.CommandTypes.Arduino_To_Interface, "Interface", "Tetris.turnRight", "Controller-Arduino");

		#endregion // Fields



		#region Properties

		// Static
		private static int _maxTabInstances;
		public static int MaxTabInstances
		{
			get { return _maxTabInstances; }
			private set
			{
				if (value < -1)
					_maxTabInstances = (int) MaxTabInstancesDefinitions.OnlyOne;
				else
					_maxTabInstances = value;
			}
		}

		private string _tetrisfilepath;
		public string TetrisFilepath
		{
			get { return _tetrisfilepath; }
			private set { _tetrisfilepath = value; OnPropertyChanged("TetrisFilepath"); }
		}

		private bool _isTetrisStarted;
		public bool IsTetrisStarted
		{
			get { return _isTetrisStarted; }
			private set { _isTetrisStarted = value; OnPropertyChanged("IsTetrisStarted"); }
		}

		#endregion // Properties



		#region Constructors

		static TetrisViewModel()
		{
			MaxTabInstances = (int) MaxTabInstancesDefinitions.OnlyOne;
		}
		
		public TetrisViewModel()
		{
			base.DisplayName = "Tetris";
			TetrisFilepath = "C:\\Users\\Benjamin\\Desktop\\Tetris 1.5.jar";
			IsTetrisStarted = false;
		}

		#endregion // Constructors



		#region Methods

		#region EventHandlers

		private void OnProcessExited(object sender, EventArgs e)
		{
			IsTetrisStarted = false;
		}

		#endregion // Event Handlers

		public void commandMoveLeft()
		{
			if (IsTetrisStarted)
			{
				press_keystroke((byte)0x25, false, false, false);
			}
		}

		public void commandMoveRight()
		{
			if (IsTetrisStarted)
			{
				press_keystroke((byte)0x27, false, false, false);
			}
		}

		public void commandMoveDown()
		{
			if (IsTetrisStarted)
			{
				press_keystroke((byte)0x28, false, false, false);
			}
		}

		public void commandTurnLeft()
		{
			if (IsTetrisStarted)
			{
				press_keystroke('a');
			}
		}

		public void commandTurnRight()
		{
			if (IsTetrisStarted)
			{
				press_keystroke('s');
			}
		}

		// Press a single key using char.
		private void press_keystroke(char c)
		{
			// Set focus to Tetris
			NativeWin32.SetForegroundWindow(process.MainWindowHandle);

			// Print Charakter
			NativeWin32.keybd_event((byte)NativeWin32.VkKeyScan(c), 0, 0, (UIntPtr) 0);				// Press
			NativeWin32.keybd_event((byte)NativeWin32.VkKeyScan(c), 0, NativeWin32.KEYEVENTF_KEYUP, (UIntPtr)0);	// Release
		}

		// Press a single key using byte and additionally shift, control and alt per boolean.
		private void press_keystroke(byte bVirtualKey, bool shift, bool control, bool alt)
		{
			// Set focus to Tetris
			NativeWin32.SetForegroundWindow(process.MainWindowHandle);

			// Press other strokes
			if (shift)
				NativeWin32.keybd_event((byte)Key.LeftShift, 0, 0, (UIntPtr)0);	// Press
			if (control)
				NativeWin32.keybd_event((byte)Key.LeftCtrl, 0, 0, (UIntPtr)0);		// Press
			if (alt)
				NativeWin32.keybd_event((byte)Key.LeftAlt, 0, 0, (UIntPtr)0);		// Press

			// Print Charakter
			NativeWin32.keybd_event(bVirtualKey, 0, 0, (UIntPtr)0);				// Press
			NativeWin32.keybd_event(bVirtualKey, 0, NativeWin32.KEYEVENTF_KEYUP, (UIntPtr)0);	// Release

			// Press other strokes
			if (shift)
				NativeWin32.keybd_event((byte)Key.LeftShift, 0, NativeWin32.KEYEVENTF_KEYUP, (UIntPtr)0);	// Release
			if (control)
				NativeWin32.keybd_event((byte)Key.LeftCtrl, 0, NativeWin32.KEYEVENTF_KEYUP, (UIntPtr)0);	// Release
			if (alt)
				NativeWin32.keybd_event((byte)Key.LeftAlt, 0, NativeWin32.KEYEVENTF_KEYUP, (UIntPtr)0);		// Release
		}

		#endregion // Methods



		#region Commands

		public ICommand StartTetris { get { return new RelayCommand(param => StartTetrisExecute(), param => CanStartTetrisExecute()); } }
		public ICommand SelectTetrisfile { get { return new RelayCommand(param => SelectTetrisfileExecute(), param => CanSelectTetrisfileExecute()); } }
		public ICommand StopTetris { get { return new RelayCommand(param => StopTetrisExecute(), param => CanStopTetrisExecute()); } }

		private void StartTetrisExecute()
		{
			try
			{
				process = Process.Start(TetrisFilepath);
			}
			catch (Exception)
			{ }

			process.EnableRaisingEvents = true;
			process.Exited += OnProcessExited;
			IsTetrisStarted = true;
		}
		private bool CanStartTetrisExecute()
		{
			if (!IsTetrisStarted)
				return true;
			else
				return false;
		}

		private void SelectTetrisfileExecute()
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "Java executables (*.jar)|*.jar";
			if (openFileDialog.ShowDialog() == true)
			{
				TetrisFilepath = openFileDialog.FileName;
			}
		}
		private bool CanSelectTetrisfileExecute()
		{
			if (!IsTetrisStarted)
				return true;
			else
				return false;
		}

		private void StopTetrisExecute()
		{
			try
			{
				process.Kill();
			}
			catch (Exception)
			{ }

			IsTetrisStarted = false;
		}
		private bool CanStopTetrisExecute()
		{
			if (IsTetrisStarted)
				return true;
			else
				return false;
		}

		#endregion



		#region Dispose

		protected override void OnDispose()
		{
			if (StopTetris.CanExecute(null))
			{
				StopTetris.Execute(null);
			}
		}

		#endregion // Dispose
	}
}
