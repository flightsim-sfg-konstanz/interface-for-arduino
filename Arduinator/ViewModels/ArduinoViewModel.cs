﻿using System;
using System.Diagnostics;
using System.Windows.Threading;

using ArduinoInterface;
using System.IO.Ports;
using System.Windows;


namespace Interface_for_Arduino.ViewModels
{
	public class ArduinoViewModel : ViewModelBase
	{
		#region Fields
		
		// Private
		private Arduino _arduino;
		private EventHandler<ArduinoInterface.ArduinoEventArgs> _messageRecievedMethod;

		private DispatcherTimer _updatePing;
		private Stopwatch _stopwatch;

		#endregion // Fields



		#region Properties

		public bool IsEnabled { get; set; }

		public string ArduinoName
		{
			get { return _arduino.Name; }
		}

		public string ComPort
		{
			get
			{
				string comPort;
				Arduino.NamesAndPorts.TryGetValue(_arduino.Name, out comPort);
				return comPort;
			}
		}

		private double _ping;

		public double Ping
		{
			get { return _ping; }
			private set { _ping = value; OnPropertyChanged("Ping"); }
		}

		public int BaudRate
		{
			get
			{
				return Arduino.BaudRate;
			}
		}

		#endregion // Properties



		#region Constructors

		public ArduinoViewModel(string name, string comPort, SerialPort sp, EventHandler<ArduinoInterface.ArduinoEventArgs> messageRecievedMethod)
		{
			try
			{
				_arduino = new Arduino(name, comPort, sp);
			}
			catch (Exception)
			{
				throw;
			}

			// Set Arduino.MessageRecieved event handler
			this._messageRecievedMethod = messageRecievedMethod;
			_arduino.MessageRecieved += this._messageRecievedMethod;

			// Intitialise timer for updating ping
			Application.Current.Dispatcher.Invoke(new Action(() =>
			{
				_updatePing = new DispatcherTimer();
			}));
				
			_updatePing.Interval = new TimeSpan(0, 0, 0, 0, 500);
			_updatePing.Tick += OnUpdatePingTick;
			_updatePing.Start();

			// Initialise stopwatch
			_stopwatch = new Stopwatch();
		}

		#endregion // Constructors



		#region Methods

		public bool Send(string command)
		{
			try
			{
				_arduino.Send(command);
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public bool Send(ArduinoCommand ardCommand)
		{
			try
			{
				_arduino.Send(ardCommand);
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		#region Event Handlers

		private void OnUpdatePingTick(object sender, EventArgs e)
		{
			Send("This is test data with 32 chars.");
			_stopwatch.Restart();
		}

		public void StopPingmeasureTime()
		{
			Ping = _stopwatch.ElapsedTicks * 1000 / (double)Stopwatch.Frequency;
			_stopwatch.Stop();
		}

		#endregion // Event Handlers

		#endregion // Methods



		#region Dispose

		protected override void OnDispose()
		{
			// Remove ping event handler
			_updatePing.Tick -= OnUpdatePingTick;

			// Remove the saved Arduino event handler
			_arduino.MessageRecieved -= _messageRecievedMethod;
			// Dispose the Arduino
			_arduino.Dispose();
		}

		#endregion // Dispose
	}
}
