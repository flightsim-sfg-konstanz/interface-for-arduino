﻿using System;
using System.Windows.Input;

namespace Interface_for_Arduino.ViewModels
{
	// Base class for every tab
	public abstract class TabViewModel : ViewModelBase
	{
		#region Fields

		private bool _tabCloseButtonEnabled;
		public bool TabCloseButtonEnabled
		{
			get { return _tabCloseButtonEnabled; }
			set
			{
				if (value != _tabCloseButtonEnabled)
				{
					_tabCloseButtonEnabled = value;
					OnPropertyChanged("TabCloseButtonEnabled");
				}
			}
		}

		public enum MaxTabInstancesDefinitions
		{
			None		= 0,
			OnlyOne		= 1,
			Unlimited	= -1
		}

		public static bool IsCountInstancesInRange(int maxTabInstances, int count)
		{
			switch (maxTabInstances)
			{
				case (int) MaxTabInstancesDefinitions.None:
					if (count == 0)
						return true;
					else
						return false;
				case (int) MaxTabInstancesDefinitions.OnlyOne:
					if (count >= 0 && count <= 1)
						return true;
					else
						return false;
				case (int) MaxTabInstancesDefinitions.Unlimited:
					if (count >= 0)
						return true;
					else
						return false;
				default:
					if (count >= 0 && count <= maxTabInstances)
						return true;
					else
						return false;
			}
		}

		#endregion // Fields

		#region Constructor

		protected TabViewModel()
		{
			TabCloseButtonEnabled = true;
		}

		#endregion // Constructor
	}
}