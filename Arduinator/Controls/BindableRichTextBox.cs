﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Interface_for_Arduino.Controls
{
	// Class derived from Leom Burke and modified to get it work by myself
	public class BindableRichTextBox : RichTextBox
	{
		public static readonly DependencyProperty DocumentProperty = DependencyProperty.Register("Document", typeof(FlowDocument), typeof(BindableRichTextBox), new PropertyMetadata(null, null, OnDocumentChanged));

		public new FlowDocument Document
		{
			get { return (FlowDocument)this.GetValue(DocumentProperty); }
			set { this.SetValue(DocumentProperty, value); }
		}

		public static object OnDocumentChanged(DependencyObject obj, object value)
		{
			if (value != null)
			{
				RichTextBox rtb = (RichTextBox)obj;

				var doc = (FlowDocument)value;
				if (doc.Tag is RichTextBox)
				{
					// clear belongs to another rtb.
					((RichTextBox)doc.Tag).Document = new FlowDocument();
				}

				doc.Tag = rtb;
				rtb.Document = doc;

				// Scroll to end
				rtb.ScrollToEnd();

				return doc;
			}

			return value;
		}
	}
}
