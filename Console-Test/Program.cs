﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ArduinoInterface;
using System.Diagnostics;
using System.Windows.Forms;

namespace Console_Test
{
	class Program
	{
		static Dictionary<string, Arduino> arduinoCollection = new Dictionary<string, Arduino>();
		static Stopwatch stopwatch = new Stopwatch();

		static void Main(string[] args)
		{
			ArduinoCommand ardcom = new ArduinoCommand(ArduinoCommand.CommandTypes.Interface_To_Arduino, "ArduinoNano", "Connection established", "Interface");
			Thread thread = new Thread(() => Clipboard.SetText(ardcom.ParseString() + ardcom.IsFaulty.ToString()));
			thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
			thread.Start();
			thread.Join();

			Arduino.ArduinoAvailable += OnArduinoAvailable;
			Arduino.ArduinoDisconnected += OnArduinoDisconnected;

			Arduino.EnableArduinoListener = true;

			do
			{
				Broadcast(Console.ReadLine());
			} while (true);
		}

		private static void Broadcast(string message)
		{
			ArduinoCommand tmpCommand;
			for (int i = 0; i < arduinoCollection.Count; i++)
			{
				tmpCommand = new ArduinoCommand(ArduinoCommand.CommandTypes.Interface_Broadcast, arduinoCollection.ElementAt(i).Key, message, "Interface");
				//Console.WriteLine(tmpCommand.ParseString());
				try
				{
					arduinoCollection.ElementAt(i).Value.Send(tmpCommand);
				}
				catch (Exception e)
				{
					PrintError("Arduino .Send() has failed\n" + e.Message);
				}
			}
		}

		private static void OnMessageRecieved(object source, ArduinoInterface.ArduinoEventArgs args)
		{
			//if (!args.Command.IsFaulty && args.Command.Origin != "Interface")
			if (!args.Command.IsFaulty)
			{
				switch (args.Command.Type)
				{
					case ArduinoCommand.CommandTypes.Arduino_To_Arduino:
						// Relay to the target Arduino
						if (arduinoCollection.ContainsKey(args.Command.Target))
						{
							try 
							{
								// Send
								arduinoCollection[args.Command.Target].Send(args.Command);
							}
							catch (Exception e)
							{
								PrintError("Arduino .Send() has failed\n" + e.Message);
							}
						}
						else
						{
							PrintError("Ziel-Arduino konnte nicht gefunden werden.\n");
						}

						Console.ForegroundColor = ConsoleColor.Blue;
						Console.Write("A-A-Data:");
						Console.ResetColor();
						Console.WriteLine(" »{0}«", args.Command);

						break;



					case ArduinoCommand.CommandTypes.Arduino_To_Interface:
						switch (args.Command.Content)
						{
							case "This is test data with 32 chars.":
								double timeElapsedMiliDecimal = stopwatch.ElapsedTicks * 1000 / (double) Stopwatch.Frequency;
								Console.WriteLine("D-time: {0:F3}ms, Data: »{1}«", timeElapsedMiliDecimal, args.Command);

								stopwatch.Restart();

								try 
								{
									// Send
									arduinoCollection[args.Command.Origin].Send("This is test data with 32 chars.");
								}
								catch (Exception e)
								{
									PrintError("Warning Arduino .send() has failed\n" + e.Message);
								}
								break;

							default:
								Console.ForegroundColor = ConsoleColor.Blue;
								Console.Write("A-I-Data:");
								Console.ResetColor();
								Console.WriteLine(" »{0}«", args.Command);
								break;
						}
						break;



					case ArduinoCommand.CommandTypes.Arduino_Broadcast:
						for (int i = 0; i < arduinoCollection.Count; i++)
						{
							if (arduinoCollection.ElementAt(i).Key != args.Command.Origin)
							{
								try
								{
									arduinoCollection.ElementAt(i).Value.Send(args.Command);
								}
								catch (Exception e)
								{
									PrintError("Warning Arduino .send() has failed\n" + e.Message);
								} 
							}
						}

						Console.ForegroundColor = ConsoleColor.Blue;
						Console.Write("A-B-Data:");
						Console.ResetColor();
						Console.WriteLine(" »{0}«", args.Command);

						break;



					default:
						break;
				}
			}
		}

		private static void OnArduinoAvailable(ArduinoChangedEventArgs args)
		{
			PrintInColor(ConsoleColor.Green, "\nA new Arduino has been detected");
			PrintInColor(ConsoleColor.Green, String.Format(" on COM-Port \"{0}\" with name {1}.", args.ComPort, args.Name));

			try
			{
				arduinoCollection.Add(args.Name, new Arduino(args.Name, args.ComPort, args.sp));
			}
			catch (Exception e)
			{
				PrintError(e.ToString());

				if (arduinoCollection.ContainsKey(args.Name))
				{
					arduinoCollection.Remove(args.Name);
				}

				Console.WriteLine();
				PrintConnectedArduinos(); 
				return;
			}

			//Task.Factory.StartNew(WaitArduinoReset).Wait();

			if (arduinoCollection.ContainsKey(args.Name))
			{
				// Add message event
				arduinoCollection[args.Name].MessageRecieved += OnMessageRecieved;
				try
				{
					arduinoCollection[args.Name].Send("Connection established");
				}
				catch (Exception)
				{
					PrintError("Connection established command was not sent");
					arduinoCollection[args.Name].Dispose();
				}
			}

			Console.WriteLine();
			PrintConnectedArduinos(); 
		}

		private static void OnArduinoDisconnected(ArduinoChangedEventArgs args)
		{
			Console.ForegroundColor = ConsoleColor.Magenta;
			Console.Write("\nDisconnection of Arduino");

			if (arduinoCollection.ContainsKey(args.Name))
			{
				arduinoCollection[args.Name].Dispose();
				arduinoCollection.Remove(args.Name);

				Console.Write(" on COM-Port \"{0}\" with name {1}.", args.ComPort, args.Name);
				Console.ResetColor();
			}

			Console.WriteLine();
			PrintConnectedArduinos();
		}

		private static void PrintConnectedArduinos()
		{
			Console.WriteLine("Connected Arduinos by Arduino collection in the main:");
			foreach (string name in arduinoCollection.Keys)
			{
				Console.WriteLine("--> {0}", name);
			}

			Console.WriteLine("Connected Arduinos by Arduino class:");

			foreach (KeyValuePair<string, string> pair in Arduino.NamesAndPorts)
			{
				Console.WriteLine("--> {0} :: {1}", pair.Key, pair.Value);
			}

			Console.WriteLine();
		}

		private static void PrintError(string error)
		{
			PrintInColor(ConsoleColor.Red, "\nFEHLER: ");
			Console.WriteLine(error);
		}

		private static void PrintInColor(ConsoleColor color, string text)
		{
			Console.ForegroundColor = color;
			Console.Write(text);
			Console.ResetColor();
		}

		public static void WaitArduinoReset()
		{
			Thread.Sleep(Arduino.ArduinoResetTime);
		}
	}
}
