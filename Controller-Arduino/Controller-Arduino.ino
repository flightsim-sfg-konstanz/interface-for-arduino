#include "ArduinoSerialInterfaceLib.h"

char arduinoName[] = "Controller-Arduino";
unsigned long baudrate = 38400;
ArduinoSerialInterface ardInterface;
bool connected;

// Pin definitions
int rxLED = 5;
int txLED = 6;

int rgbLED_r = 9;
int rgbLED_g = 10;
int rgbLED_b = 11;

int tasterRS_R = A0;
int tasterRS_L = A1;
int taster_U = A2;
int taster_L = A3;
int taster_R = A4;
int taster_D = A5;

int schalterS_L = 3;
int schalterS_R = 4;
int schalterB = 2;

// Changed states
bool state_tasterRS_R;
bool state_tasterRS_L;
bool state_taster_U;
bool state_taster_L;
bool state_taster_R;
bool state_taster_D;

bool state_schalterS_L;
bool state_schalterS_R;
bool state_schalterB;

// the setup function runs once when you press reset or power the board
void setup() {
	// Set up the interface
	connected = false;
	ardInterface.init(arduinoName, baudrate);

	// Set up pins
	pinMode(rxLED, OUTPUT);
	pinMode(txLED, OUTPUT);
	pinMode(rgbLED_r, OUTPUT);
	pinMode(rgbLED_g, OUTPUT);
	pinMode(rgbLED_b, OUTPUT);

	pinMode(tasterRS_R, INPUT_PULLUP);
	pinMode(tasterRS_L, INPUT_PULLUP);
	pinMode(taster_U, INPUT_PULLUP);
	pinMode(taster_L, INPUT_PULLUP);
	pinMode(taster_R, INPUT_PULLUP);
	pinMode(taster_D, INPUT_PULLUP);

	pinMode(schalterS_L, INPUT_PULLUP);
	pinMode(schalterS_R, INPUT_PULLUP);
	pinMode(schalterB, INPUT_PULLUP);


	// Initalise changed states
	state_tasterRS_R = digitalRead(tasterRS_R);
	state_tasterRS_L = digitalRead(tasterRS_L);
	state_taster_U = digitalRead(taster_U);
	state_taster_L = digitalRead(taster_L);
	state_taster_R = digitalRead(taster_R);
	state_taster_D = digitalRead(taster_D);

	state_schalterS_L = digitalRead(schalterS_L);
	state_schalterS_R = digitalRead(schalterS_R);
	state_schalterB = digitalRead(schalterB);
}

// the loop function runs over and over again until power down or reset
void loop() {
	if (ardInterface.read_serial()) {
		if (ardInterface.command.type == "I-A"  && ardInterface.command.content == "Connection established" && ardInterface.command.origin == "Interface") {
			connected = true;
			analogWrite(txLED, 255);
		}
		else if (ardInterface.command.type == "I-A" && ardInterface.command.content == "This is test data with 32 chars." && ardInterface.command.origin == "Interface") {
			ardInterface.write_serial({ "A-I", "Interface", "This is test data with 32 chars.", arduinoName });
		}
		else {
			String content = "$the command from (" + ardInterface.command.origin + ") isn't known.";
			ardInterface.write_serial({ "A-I", "Interface", content, arduinoName });
		}

		// LEDs einstellen
		if ((ardInterface.command.type == "I-A" || ardInterface.command.type == "A-I") && ardInterface.command.content != "This is test data with 32 chars.")
			setColorToLED(0, 0, 128);
		else if (ardInterface.command.type == "A-A")
			setColorToLED(0, 128, 0);
		else if (ardInterface.command.type == "I-B" || ardInterface.command.type == "A-B")
			setColorToLED(128, 30, 0);
	}

	if (connected) {

		// Taster verarbeiten
		bool current_state_tasterRS_R = digitalRead(tasterRS_R);
		bool current_state_tasterRS_L = digitalRead(tasterRS_L);
		bool current_state_taster_U = digitalRead(taster_U);
		bool current_state_taster_L = digitalRead(taster_L);
		bool current_state_taster_R = digitalRead(taster_R);
		bool current_state_taster_D = digitalRead(taster_D);

		bool current_state_schalterS_L = digitalRead(schalterS_L);
		bool current_state_schalterS_R = digitalRead(schalterS_R);
		bool current_state_schalterB = digitalRead(schalterB);

		if (state_tasterRS_R != current_state_tasterRS_R) {
			if (current_state_tasterRS_R == LOW)
				ardInterface.write_serial({ "A-I", "Interface", "Tetris.moveRight", arduinoName });
			state_tasterRS_R = current_state_tasterRS_R;
		}

		if (state_tasterRS_L != current_state_tasterRS_L) {
			if (current_state_tasterRS_L == LOW)
				ardInterface.write_serial({ "A-I", "Interface", "Tetris.moveLeft", arduinoName });
			state_tasterRS_L = current_state_tasterRS_L;
		}

		if (state_taster_U != current_state_taster_U) {
			if (current_state_taster_U == LOW)
				ardInterface.write_serial({ "A-B", "unspecified", "Relay to self", arduinoName });
			state_taster_U = current_state_taster_U;
		}

		if (state_taster_L != current_state_taster_L) {
			if (current_state_taster_L == LOW)
				ardInterface.write_serial({ "A-I", "Interface", "Tetris.turnLeft", arduinoName });
			state_taster_L = current_state_taster_L;
		}

		if (state_taster_R != current_state_taster_R) {
			if (current_state_taster_R == LOW)
				ardInterface.write_serial({ "A-I", "Interface", "Tetris.turnRight", arduinoName });
			state_taster_R = current_state_taster_R;
		}

		if (state_taster_D != current_state_taster_D) {
			if (current_state_taster_D == LOW)
				ardInterface.write_serial({ "A-I", "Interface", "Tetris.moveDown", arduinoName });
			state_taster_D = current_state_taster_D;
		}


		if (state_schalterS_L != current_state_schalterS_L || state_schalterS_R != current_state_schalterS_R) {
			if (current_state_schalterS_L == LOW) {
				ardInterface.write_serial({ "A-I", "Interface", "MSchalter.right", arduinoName });
			}
			else if (current_state_schalterS_R == LOW) {
				ardInterface.write_serial({ "A-I", "Interface", "MSchalter.left", arduinoName });
			}
			else {
				ardInterface.write_serial({ "A-I", "Interface", "MSchalter.center", arduinoName });
			}

			state_schalterS_L = current_state_schalterS_L;
			state_schalterS_R = current_state_schalterS_R;
		}

		if (state_schalterB != current_state_schalterB) {
			if (current_state_schalterB == LOW) {
				ardInterface.write_serial({ "A-I", "Interface", "LSchalter.up", arduinoName });
			}
			else {
				ardInterface.write_serial({ "A-A", "ArdruinoMega", "testA-A", arduinoName });
			}

			state_schalterB = current_state_schalterB;
		}
	}
}

void setColorToLED(byte red, byte green, byte blue) {
	analogWrite(rgbLED_r, red);
	analogWrite(rgbLED_g, green);
	analogWrite(rgbLED_b, blue);
}