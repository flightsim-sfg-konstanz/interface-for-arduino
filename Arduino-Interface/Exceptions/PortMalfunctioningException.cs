﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArduinoInterface.Exceptions
{
	[Serializable]
	public class PortMalfunctioningException : Exception
	{
		public PortMalfunctioningException() { }
		public PortMalfunctioningException(string message) : base(message) { }
		public PortMalfunctioningException(string message, Exception inner) : base(message, inner) { }
		protected PortMalfunctioningException(
			System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context)
			: base(info, context) { }
	}
}
