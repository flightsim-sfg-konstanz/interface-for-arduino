﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Management;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using ArduinoInterface;
using System.Collections.Concurrent;

namespace ArduinoInterface
{
	[Export]
	[PartCreationPolicy(CreationPolicy.Shared)]
	public sealed class SerialPortWatcher : IDisposable
	{
		#region Members

		private List<string> _comPorts;

		private ManagementEventWatcher _watcher;
		private TaskScheduler _taskScheduler;

		#endregion



		#region EventDefinitions

		// SerialPortAdded
		public delegate void SerialPortAddedEventHandler(object sender, SerialPortChangedEventArgs args);
		public event SerialPortAddedEventHandler SerialPortAdded;

		// SerialPordRemoved
		public delegate void SerialPortRemovedEventHandler(object sender, SerialPortChangedEventArgs args);
		public event SerialPortAddedEventHandler SerialPortRemoved;

		#endregion



		#region Constructors

		public SerialPortWatcher()
		{
			_taskScheduler = TaskScheduler.Default;
			_comPorts = new List<string>(SerialPort.GetPortNames());

			WqlEventQuery query = new WqlEventQuery("SELECT * FROM Win32_DeviceChangeEvent");

			_watcher = new ManagementEventWatcher(query);
			_watcher.EventArrived += (sender, eventArgs) => OnDeviceChanged(eventArgs);
			_watcher.Start();
		}

		#endregion



		#region Methods

		private void OnDeviceChanged(EventArrivedEventArgs args)
		{
			UInt16 EventType = (UInt16)args.NewEvent.GetPropertyValue("EventType");
			if (EventType == 2 || EventType == 3)
			{
				CheckPorts();
			}
		}

		private void CheckPorts()
		{
			IEnumerable<string> currentComPorts = SerialPort.GetPortNames();

			string[] tmpComPorts = _comPorts.ToArray();
			foreach (string comPort in tmpComPorts)
			{
				if (!currentComPorts.Contains(comPort))
				{
					_comPorts.Remove(comPort);
					RaiseSerialPortRemoved(comPort);
				}
			}

			foreach (string comPort in currentComPorts)
			{
				if (!_comPorts.Contains(comPort))
				{
					_comPorts.Add(comPort);
					Task.Factory.StartNew(() => RaiseSerialPortAdded(comPort), CancellationToken.None, TaskCreationOptions.LongRunning, _taskScheduler);
				}
			}
		}

		#region EventRaiseMethods

		private void RaiseSerialPortAdded(string comPort)
		{
			if (SerialPortAdded != null)
			{
				SerialPortAdded(this, new SerialPortChangedEventArgs() { ComPort = comPort });
			}
		}
		private void RaiseSerialPortRemoved(string comPort)
		{
			if (SerialPortRemoved != null)
			{
				SerialPortRemoved(this, new SerialPortChangedEventArgs() { ComPort = comPort });
			}
		}

		#endregion

		#endregion



		#region Dispose

		public void Dispose()
		{
			_watcher.Stop();
			_watcher.Dispose();
		}

		#endregion // Dispose
	}
}