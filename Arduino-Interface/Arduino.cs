﻿using ArduinoInterface.Exceptions;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ArduinoInterface
{
	public class Arduino : IDisposable
	{
		#region Fields

		public const int ArduinoResetTime = 2000;
		public const int BaudRate = 38400;

		public readonly string Name;

		private static SerialPortWatcher _serialPortWatcher;
		private SerialPort _sp;

		private StringBuilder _tempSerialData;

		#endregion // Fields



		#region Event Definitions

		public event EventHandler<ArduinoEventArgs> MessageRecieved;

		public delegate void ArduinoAvailableEventHandler(ArduinoChangedEventArgs args);
		public static event ArduinoAvailableEventHandler ArduinoAvailable;
		public delegate void ArduinoDisconnectedEventHandler(ArduinoChangedEventArgs args);
		public static event ArduinoDisconnectedEventHandler ArduinoDisconnected;

		#endregion



		#region Properties

		private static bool _enableArduinoListener = false;
		public static bool EnableArduinoListener
		{
			get { return _enableArduinoListener; }
			set
			{
				if (value != _enableArduinoListener)
				{
					// If set on
					if (value == true)
					{
						_serialPortWatcher = new SerialPortWatcher();

						// Register events
						_serialPortWatcher.SerialPortAdded += OnSerialPortAdded;
						_serialPortWatcher.SerialPortRemoved += OnSerialPortRemoved;

						// Check async if any Arduino has already been connected
						Task<bool>.Factory.StartNew(CheckAllConnected, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
					}

					// If set off
					else
					{
						_serialPortWatcher.Dispose();
					} 

					// Set the value
					_enableArduinoListener = value; 
				}
			}
		}

		private static Dictionary<string, string> _namesAndPorts = new Dictionary<string, string>();
		public static Dictionary<string,string> NamesAndPorts
		{
			get { return _namesAndPorts; }
			private set { _namesAndPorts = value; }
		}

		private bool _isDisposed = false;
		public bool IsDisposed
		{
			get { return _isDisposed; }
		}
		
		#endregion



		#region Constructors/Destructor/Dispose

		public Arduino(string name, string comPort)
		{
			_sp = new SerialPort(comPort);
			_sp.BaudRate = BaudRate;
			_sp.DtrEnable = false;

			// Try to open the COM-port
			try
			{
				_sp.Open();
			}
			catch (Exception)
			{
				// Must cleanup ressources.
				this.Dispose();
				// Relay exception to the application.
				throw;
			}

			// Add event for incomming data
			_sp.DataReceived += DataReceivedHandler;

			_tempSerialData = new StringBuilder();

			NamesAndPorts.Add(name, comPort);
			Name = name;
		}

		public Arduino(string name, string comPort, SerialPort sp)
		{
			_sp = sp;

			if (!_sp.IsOpen || !SerialPort.GetPortNames().Contains(comPort))
			{
				// Must cleanup ressources.
				this.Dispose();
				// Relay exception to the application.
				throw new PortMalfunctioningException("The Port the Arduino is not Open or was already disconnected");
			}

			// Add event for incomming data
			_sp.DataReceived += DataReceivedHandler;

			_tempSerialData = new StringBuilder();

			NamesAndPorts.Add(name, comPort);
			Name = name;
		}

		~Arduino()
		{
			OnDispose(false);
		}

		public void Dispose()
		{
			OnDispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void OnDispose(bool disposing)
		{
			if (!_isDisposed)
			{
				if (disposing)
				{
					// release managed resources:
					if (Name != null)
					{
						if (NamesAndPorts.ContainsKey(Name))
						{
							NamesAndPorts.Remove(Name);
						} 
					}
				}

				// release unmanaged resources, if any
				try
				{
					_sp.Close();
					_sp.Dispose();
				}
				catch (Exception)
				{
					// Simply fuck, fuck, fuck, fuck, fuck you Microsoft ... your damn shitty code you programmed -.-
				}

				_isDisposed = true;
			}
		}

		#endregion // Constructors/Destructor/Dispose



		#region Methods

		// Send a command to the Arduino instance.
		public void Send(string command)
		{
			ArduinoCommand ardCommand = new ArduinoCommand(ArduinoCommand.CommandTypes.Interface_To_Arduino, Name, command, "Interface");

			try
			{
				// Beware of the Krakens -1 ... you ******* ***** Microsoft.
				_sp.Write(ardCommand.ParseString());
				//Console.WriteLine("Sent this:" + ardCommand.ParseString());
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Send(ArduinoCommand ardCommand)
		{
			try
			{
				// Beware of the Krakens -1 ... you ******* ***** Microsoft.
				_sp.Write(ardCommand.ParseString());
				//Console.WriteLine("Sent this:" + ardCommand.ParseString());
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		#region Event Handlers

		private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
		{
			do
			{
				char c = (char) _sp.ReadChar();
				_tempSerialData.Append(c);

				//Console.WriteLine("data:{0}", c);

				if (_tempSerialData.ToString().EndsWith(";"))
				{
					RaiseMessageRecieved(new ArduinoCommand(_tempSerialData.ToString()));
					//Console.WriteLine(">>"+_tempSerialData.ToString()+"<<");
					_tempSerialData.Clear();
				}
			} while (_sp.BytesToRead != 0);
		}

		private static void OnSerialPortAdded(object sender, SerialPortChangedEventArgs args)
		{
			CheckArduinoAvailable(args.ComPort);
		}
		private static void OnSerialPortRemoved(object sender, SerialPortChangedEventArgs args)
		{
			CheckArduinoDisconnected(args.ComPort);
		}

		#endregion // Event Handlers

		#region EventRaiseMethods

		protected virtual void RaiseMessageRecieved(ArduinoCommand command)
		{
			if (MessageRecieved != null && NamesAndPorts.ContainsKey(Name))
			{
				MessageRecieved(this, new ArduinoEventArgs() { Command = command, ComPort = NamesAndPorts[Name]});
			}
		}

		protected static void RaiseArduinoAvailable(KeyValuePair<string, string> nameAndPort, SerialPort sp)
		{
			if (ArduinoAvailable != null)
			{
				ArduinoAvailable(new ArduinoChangedEventArgs() { Name = nameAndPort.Key, ComPort = nameAndPort.Value, sp = sp });
			}
		}
		protected static void RaiseArduinoDisconnected(KeyValuePair<string, string> nameAndPort)
		{
			if (ArduinoDisconnected != null)
			{
				ArduinoDisconnected(new ArduinoChangedEventArgs() { Name = nameAndPort.Key, ComPort = nameAndPort.Value });
			}
		}

		#endregion

		#region CheckArduino

		// Iterates through all available COM-ports and calls the CheckArduinoAvailable method with it.
		public static bool CheckAllConnected()
		{
			bool ret = false;
			foreach (string comPort in SerialPort.GetPortNames())
			{
				if (!NamesAndPorts.Values.Contains(comPort))
				{
					if (CheckArduinoAvailable(comPort)) { ret = true; }
				}
			}
			return ret;
		}

		// Iterates through all connected Arduino-Com-ports hold in this class and compares them with
		// the currently connected COM-ports in SerialPort.GetPortNames()
		public static bool CheckAllDisconnected()
		{
			bool ret = false;
			string[] connectedArduinoPorts = NamesAndPorts.Values.ToArray();
			for (int i = 0; i < connectedArduinoPorts.Length; i++)
			{
				if (CheckArduinoDisconnected(connectedArduinoPorts[i])) { ret = true; }
			}
			return ret;
		}

		// Creates the event ArduinoAvailable if any Arduino has been found that is ready to connect.
		// Caution, contains long operation / Thread.Sleep(ArduinoResetTime);
		public static bool CheckArduinoAvailable(string comPort)
		{
			if (!NamesAndPorts.Values.Contains(comPort) && SerialPort.GetPortNames().Contains(comPort))
			{
				SerialPort sp = new SerialPort(comPort);       

				sp.BaudRate = BaudRate;
				sp.DtrEnable = true;

				if (!sp.IsOpen)
				{
					string read = "";
					string name = "";

					try
					{
						// Caution, long operation!
						sp.Open();
                        GC.SuppressFinalize(sp.BaseStream); // Again f**********ck you Microsoft
					}
					catch (Exception e)
					{
                        Console.WriteLine(comPort);
                        sp.Close();
                        sp.Dispose();
						return false;
					}

					Thread.Sleep(ArduinoResetTime);

					try
					{
						read = sp.ReadExisting();
					}
					catch (Exception e)
					{
						Console.WriteLine(e);
						return false;
					}

					Console.WriteLine(read);

					// Format names
					if (read.StartsWith("Name<") && read.EndsWith(">;"))
					{
						name = read.Substring(5, read.Length - 7);
					}
					else
					{
						return false;
					}

					// Only send event if the name is not empty and is not already connected
					if (name != "" && !NamesAndPorts.Keys.Contains(name) && sp.IsOpen)
					{
						// Create the event.
						RaiseArduinoAvailable(new KeyValuePair<string, string>(name, comPort), sp);
						return true;
					} 
				}
			}
			
			return false;
		}

		// Creates the event ArduinoDisconnected if any Arduino has become unavailable to the COM-Ports.
		public static bool CheckArduinoDisconnected(string comPort)
		{
			Console.WriteLine("Checking for disconnected Devices");
			if (NamesAndPorts.Values.Contains(comPort) && !SerialPort.GetPortNames().Contains(comPort))
			{
				Console.WriteLine("Working...");
				// Create variable to hold the name and COM-port of the Arduino.
				KeyValuePair<string, string> nameAndPort = NamesAndPorts.ElementAt(NamesAndPorts.Values.ToList().IndexOf(comPort));

				// Create the event that the Arduino has been disconnected.
				RaiseArduinoDisconnected(nameAndPort);

				// Remove the COM-port from the list if there is any.
				if (NamesAndPorts.ContainsValue(comPort))
				{
					NamesAndPorts.Remove(nameAndPort.Key);
				}

				return true;
			}

			return false;
		}

		#endregion

		#endregion
	}
}
