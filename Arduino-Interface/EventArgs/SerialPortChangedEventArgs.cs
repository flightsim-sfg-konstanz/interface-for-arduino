﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArduinoInterface
{
	public class SerialPortChangedEventArgs
	{
		public string ComPort { get; set; }
	}
}
