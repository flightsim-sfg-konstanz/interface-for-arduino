﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArduinoInterface
{
	public class ArduinoChangedEventArgs : EventArgs
	{
		public string Name { get; set; }
		public string ComPort { get; set; }
		public SerialPort sp { get; set; }
	}
}
