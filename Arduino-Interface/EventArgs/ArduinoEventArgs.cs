﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArduinoInterface
{
	public class ArduinoEventArgs : EventArgs
	{
		public ArduinoCommand Command { get; set; }
		public string ComPort { get; set; }
	}
}
