﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArduinoInterface
{
	public class ArduinoCommand
	{
		#region Fields

		public enum CommandTypes
		{
			Faulty = -1,				// Set if the command is Faulty

			Arduino_To_Arduino = 0,		// Arduino --send--> Arduino
			Arduino_To_Interface = 1,	// Arduino --send--> Interface
			Interface_To_Arduino = 2,	// Interface --send--> Arduino
			//Arduino_Multicom = 3,		// Arduino --send--> Multiple targets
			//Interface_Multicom = 4,	// Interface --send--> Multiple targets
			Arduino_Broadcast = 5,		// Arduino --send--> All known targets
			Interface_Broadcast = 6		// Interface --send--> All known Arduinoss.
		}

		#endregion



		#region Properties

		private bool _isFaulty;		// If the command is valid
		public bool IsFaulty
		{
			get { return _isFaulty; }
			private set { _isFaulty = value; }
		}

		private CommandTypes _type;	// Message type
		public CommandTypes Type
		{
			get { return _type; }
			private set { _type = value; }
		}

		private string _target;		// Target of the message
		public string Target
		{
			get { return _target; }
			private set { _target = value; }
		}

		private string _content;	// Message content
		public string Content
		{
			get { return _content; }
			private set { _content = value; }
		}

		private string _origin;		// Origin of the message
		public string Origin
		{
			get { return _origin; }
			private set { _origin = value; }
		}

		#endregion



		#region Constructors

		public ArduinoCommand(CommandTypes type, string target, string command, string origin)
		{
			// Set properties
			Type = type;
			if (target != "" && command != "" && origin != "")
			{
				Target = target.Trim();
				Content = command.Trim();
				Origin = origin.Trim(); 
			}

			// Verify that the command fulfills the specification
			VerifyFaultiness();
		}

		public ArduinoCommand(string command)
		{
			// Set properties.
			SetCommandByString(command);
			// Check for errors.
			VerifyFaultiness();
		}

		public ArduinoCommand(byte[] bytes)
		{
			// Convert back to string
			string command = System.Text.Encoding.ASCII.GetString(bytes);
			// Set properties
			SetCommandByString(command);
			// Check for errors
			VerifyFaultiness();
		}

		#endregion



		#region Methods

		// Processes a string into the properties of the current instance.
		// Specification of a string:
		// Type<t>::Target<tgt>::Content<cont>::Origin<orig>;
		private void SetCommandByString(string command)
		{
			// Split the string at the delimiter ::
			string[] parts = command.Split(new string[] { "::" }, StringSplitOptions.None);

			// Set the type
			if (parts.Length == 4)
			{
				// Set the command type.
				switch (parts[0].Substring(5, parts[0].Length - 6))
				{
					case "A-A":
						Type = CommandTypes.Arduino_To_Arduino;
						break;
					case "A-I":
						Type = CommandTypes.Arduino_To_Interface;
						break;
					case "I-A":
						Type = CommandTypes.Interface_To_Arduino;
						break;
					case "A-B":
						Type = CommandTypes.Arduino_Broadcast;
						break;
					case "I-B":
						Type = CommandTypes.Interface_Broadcast;
						break;
					default:
						_isFaulty = true;
						break;
				}

				// Set strings by Substring functions and trimm them.
				Target = parts[1].Substring(7, parts[1].Length - 8).Trim();
				Content = parts[2].Substring(8, parts[2].Length - 9).Trim();
				Origin = parts[3].Substring(7, parts[3].Length - 9).Trim();
			}
		}

		// Use to send byte arrays
		public byte[] ParseByte()
		{
			return System.Text.Encoding.ASCII.GetBytes(ToString());
		}

		// Use to send data via string
		public string ParseString()
		{
			return ToString();
		}

		// Returns true if an error in the instance has been found.
		public bool VerifyFaultiness()
		{
			if (!Enum.IsDefined(typeof(CommandTypes), Type))
			{
				IsFaulty = true;
			}

			if (Target == "Interface" && Type != CommandTypes.Arduino_To_Interface)
			{
				IsFaulty = true;
			}

			if (Target != "unspecified" && (Type == CommandTypes.Arduino_Broadcast || Type == CommandTypes.Interface_Broadcast))
			{
				IsFaulty = true;
			}

			if (Target == null || Content == null || Origin == null)
			{
				IsFaulty = true;
			}

			// Return if Faulty
			if (IsFaulty)
			{
				setFaulty();
				return true;
			}
			else
			{
				return false;
			}
		}

		// Sets the command to a faulty state
		private void setFaulty()
		{
			Type = CommandTypes.Faulty;
			Target = "unknown";
			Content = "none";
			Origin = "unknown";
		}

		public override string ToString()
		{
			// Hold the current type in a string
			string type = "";
			switch (Type)
			{
				case CommandTypes.Faulty:
					type = "Faulty";
					break;
				case CommandTypes.Arduino_To_Arduino:
					type = "A-A";
					break;
				case CommandTypes.Arduino_To_Interface:
					type = "A-I";
					break;
				case CommandTypes.Interface_To_Arduino:
					type = "I-A";
					break;
				case CommandTypes.Arduino_Broadcast:
					type = "A-B";
					break;
				case CommandTypes.Interface_Broadcast:
					type = "I-B";
					break;
			}

			return string.Format("Type<{0}>::Target<{1}>::Content<{2}>::Origin<{3}>;", type, Target, Content, Origin);
		}

		#endregion
	}
}
