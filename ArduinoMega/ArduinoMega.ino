#include "ArduinoSerialInterfaceLib.h"

char arduinoName[] = "ArduinoMega";
unsigned long baudrate = 38400;
ArduinoSerialInterface ardInterface;

bool switchState1;

// the setup function runs once when you press reset or power the board
void setup() {
	// Set up the interface
	ardInterface.init(arduinoName, baudrate);

	pinMode(13, OUTPUT);

	pinMode(50, INPUT_PULLUP);

	switchState1 = digitalRead(50);
}

// the loop function runs over and over again until power down or reset
void loop() {
	if (ardInterface.read_serial()) {
		if (ardInterface.command.type == "I-A"  && ardInterface.command.content == "Connection established" && ardInterface.command.origin == "Interface") {

		}
		else if (ardInterface.command.type == "I-A" && ardInterface.command.content == "This is test data with 32 chars." && ardInterface.command.origin == "Interface") {
			ardInterface.write_serial({ "A-I", "Interface", "This is test data with 32 chars.", arduinoName });
		}
		else if (ardInterface.command.type == "A-B" && ardInterface.command.content == "Relay to self" && ardInterface.command.origin == "Controller-Arduino") {
			ardInterface.write_serial({ "A-A", "Controller-Arduino", "LED=on", arduinoName });
		}
		else if (ardInterface.command.type == "A-A" && ardInterface.command.content == "testA-A" && ardInterface.command.origin == "Controller-Arduino") {
			digitalWrite(13, HIGH);
		}

		else if (ardInterface.command.type == "I-B" && ardInterface.command.content == "Broadcast!") {
			digitalWrite(13, HIGH);
		}
		else {
			String content = "$the command from (" + ardInterface.command.origin + ") isn't known.";
			ardInterface.write_serial({ "A-I", "Interface", content, arduinoName });
		}
	}

	if (switchState1 != digitalRead(50)) {
		ardInterface.write_serial({ "A-B", "unspecified", "Miau", arduinoName });
		switchState1 = digitalRead(50);
	}
}
