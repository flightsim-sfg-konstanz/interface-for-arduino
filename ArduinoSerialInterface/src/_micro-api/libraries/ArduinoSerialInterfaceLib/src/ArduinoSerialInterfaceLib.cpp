#include "ArduinoSerialInterfaceLib.h"

// Command with null values.
ArduinoSerialInterface::ArduinoCommand nullCmd;

ArduinoSerialInterface::ArduinoSerialInterface() { }

void ArduinoSerialInterface::init(char arduinoName[], unsigned long baudrate) {
	_arduinoName = arduinoName;
	Serial.begin(baudrate);
	while (!Serial) {
		; // wait for serial port to connect. Needed for native USB
	}
	Serial.print("Name<" + _arduinoName + ">;");
}

bool ArduinoSerialInterface::read_serial() {
	// Set up temporary Variables
	char c;

	while (Serial.available() > 0) {

		char c = Serial.read();

		// Fuck you *******, I hate you Microsoft you goddamn piece of ****.
		if ((int)c != -1) {
			_tmpInData.concat(c);   // Add character to the string 
		}

		if (c == ';') {
			// Remove whitespaces
			_tmpInData.trim();
			inData = _tmpInData;

			// Type<I-A>::Target<ArduinoMega>::Content<Connection established>::Origin<Interface>;
			command.type = inData.substring(5, 8);
			command.target = inData.substring(inData.indexOf("Target<") + 7, inData.indexOf(">::Content"));
			command.content = inData.substring(inData.indexOf("Content<") + 8, inData.indexOf(">::Origin"));
			command.origin = inData.substring(inData.indexOf("Origin<") + 7, inData.indexOf(">;"));

			_tmpInData = "";
			return true;
		}
	}
}

void ArduinoSerialInterface::write_serial(ArduinoCommand sendCmd) {
	// If there is something to write
	if (sendCmd.type != nullCmd.type && sendCmd.target != nullCmd.target && sendCmd.content != nullCmd.content && sendCmd.origin != nullCmd.origin) {
		Serial.print("Type<" + sendCmd.type + ">::Target<" + sendCmd.target + ">::Content<" + sendCmd.content + ">::Origin<" + sendCmd.origin + ">;");
	}
}

// Writes a string to serial and sets the string to nothing
void ArduinoSerialInterface::write_serial(String &data) {
	// If there is something to write
	if (data != "") {
		Serial.print(data);
	}
}