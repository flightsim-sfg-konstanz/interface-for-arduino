#ifndef _ArduinoSerialInterfaceLib_h
#define _ArduinoSerialInterfaceLib_h

#include "arduino.h"

class ArduinoSerialInterface
{

public:
	// Fields
	String inData = "";

	struct ArduinoCommand {
		String type;
		String target;
		String content;
		String origin;
	};
	ArduinoCommand command = { "", "", "", "" };

	// Methods
	ArduinoSerialInterface();
	void init(char arduinoName[], unsigned long baudrate);
	bool read_serial();
	void write_serial(ArduinoCommand sendCmd);
	void write_serial(String &data);

private:
	String _arduinoName;
	String _tmpInData = "";
};

#endif

